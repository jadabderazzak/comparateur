<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface  $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for($i = 0; $i < 10 ; $i++)
        {

            $user = new User();
            $user
                    ->setNom($faker->firstName())
                    ->setEmail($faker->companyEmail())
                    ->setPassword($this->encoder->hashPassword($user,'admin'))
                   
                    ;
                  
            $manager->persist($user);


        }

        $manager->flush();
    }
}
