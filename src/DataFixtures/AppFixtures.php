<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
       

        /********************************* Category  */

        for($i = 0; $i < 10 ; $i++)
        {

            $category = new Category();
            $category->setNom($faker->word());
                  
            $manager->persist($category);


        }
        $manager->flush();

        $category = $manager->getRepository(Category::class)->find($faker->numberBetween(1, 10));
        /*************************** Articles */

            for($i = 0; $i < 100 ; $i++)
            {

                $article = new Article();
                $article->setNom($faker->words(4, true))
                        ->setDescription($faker->text())
                        ->setLien($faker->url())
                        ->setPrix($faker->randomFloat(2, 200, 15000))
                        ->setDetails($faker->paragraph())
                        ->setCategory($category)
                        ->setImageName('pc-5-64941fca6cd44992754591.png')
                        ;
                      
                $manager->persist($article);


            }

          

        $manager->flush();
    }
}
