<?php

namespace App\Model;

use App\Entity\Category;


class RechercheDonnee {

    /** @var int */
    public $page = 1;

     /** @var string */
     public $mot = '';

     /** @var Category|null */
     public $category ;
}