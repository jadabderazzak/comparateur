<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity('email', message: 'Cet Email existe déjà.')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    private $sluger;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Assert\NotBlank(message: 'L\'adresse Email ne doit pas être vide!')]
    #[Assert\Email(
        message: 'l\'Email {{ value }} n\'est pas valide',
    )]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Le nom ne doit pas être vide!')]
    #[Assert\Length(
        min : 3, 
        max : 200,
        minMessage : 'Vous devez saisir au moins 3 caractères',
        maxMessage : 'Vous ne devez pas dépasser 200 caractères'
        )]
    private ?string $nom = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Supplierarticle::class)]
    private Collection $supplierarticles;

    public function __construct()
    {
        $this->sluger =   new AsciiSlugger();
        $this->supplierarticles = new ArrayCollection();
      
    }

    #[ORM\PrePersist]
    public function prePersist()
    {
        $this->slug = $this->sluger->slug($this->getNom());
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Supplierarticle>
     */
    public function getSupplierarticles(): Collection
    {
        return $this->supplierarticles;
    }

    public function addSupplierarticle(Supplierarticle $supplierarticle): static
    {
        if (!$this->supplierarticles->contains($supplierarticle)) {
            $this->supplierarticles->add($supplierarticle);
            $supplierarticle->setUser($this);
        }

        return $this;
    }

    public function removeSupplierarticle(Supplierarticle $supplierarticle): static
    {
        if ($this->supplierarticles->removeElement($supplierarticle)) {
            // set the owning side to null (unless already changed)
            if ($supplierarticle->getUser() === $this) {
                $supplierarticle->setUser(null);
            }
        }

        return $this;
    }
}
