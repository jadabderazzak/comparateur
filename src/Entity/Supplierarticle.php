<?php

namespace App\Entity;

use App\Repository\SupplierarticleRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SupplierarticleRepository::class)]
class Supplierarticle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Article $article = null;

    #[ORM\ManyToOne(inversedBy: 'supplierarticles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column]
    #[Assert\NotBlank(message: 'Le prix ne doit pas être vide!')]
    #[Assert\Positive(message: 'Le prix doit être un nombre valide')]
    private ?float $prix = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Le lien ne doit pas être vide!')]
    #[Assert\Url(message: 'Veuillez introsuire une adresse URL valide. http://www.example.com ')]
    private ?string $lien = null;

    #[ORM\Column]
    private ?bool $livraison = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Assert\Positive(message: 'Le prix de la livraison doit être un nombre valide')]
    private ?int $prixLivraison = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): static
    {
        $this->article = $article;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    public function getLien(): ?string
    {
        return $this->lien;
    }

    public function setLien(string $lien): static
    {
        $this->lien = $lien;

        return $this;
    }

    public function isLivraison(): ?bool
    {
        return $this->livraison;
    }

    public function setLivraison(bool $livraison): static
    {
        $this->livraison = $livraison;

        return $this;
    }

    public function getPrixLivraison(): ?int
    {
        return $this->prixLivraison;
    }

    public function setPrixLivraison(?int $prixLivraison): static
    {
        $this->prixLivraison = $prixLivraison;

        return $this;
    }
}
