<?php

namespace App\Controller;

use App\Form\SearchFormType;
use App\Model\RechercheDonnee;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home.index')]
    public function index(CategoryRepository $repoCats, Request $request, ArticleRepository $repoArticles, CategoryRepository $repoCat, PaginatorInterface $paginator): Response
    {
        $rechercheDonnee = new RechercheDonnee();
        $categories = $repoCats->findAll();
        $form = $this->createForm(SearchFormType::class,$rechercheDonnee);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
           
             // Récuperation de l'id de l'etat depuis le request
             $catId = $request->get('search_form')['category'];
             $category = $catId !== "" ? $repoCat->findOneBy(['id'=> $catId]) : null;
            
             $rechercheDonnee->page = $request->query->getInt('page', 1);
             $req = $repoArticles->findByName($rechercheDonnee, $category);
             $total = count($req);
             $articles = $paginator->paginate(
                $req,
                $request->query->get('page', 1),
                8
            );
          

            return $this->render('home/index.html.twig',[
                'total' => $total,
                'categories' => $categories,
                'articles' => $articles,
                'form' => $form->createView()
            ]);
      

        }else{

           
            $articles = $repoArticles->findBy([],[
                'id' => 'DESC'
            ]);
            $total = count($articles);

            $articles = $paginator->paginate(
                $articles,
                $request->query->get('page', 1),
                8
            );

        }

        return $this->render('home/index.html.twig',[
            'total' => $total,
            'categories' => $categories,
            'articles' => $articles,
            'form' => $form->createView()
        ]);
    }
}
