<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[IsGranted('ROLE_USER')]
class CategoryController extends AbstractController
{
    #[Route('/category', name: 'category.index')]
    public function index(CategoryRepository $repoCategory): Response
    {
        return $this->render('category/index.html.twig');
    }

    #[Route('/category/add', name: 'category.add')]
    public function add(Request $request, EntityManagerInterface $manager): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class,$category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
           $category = $form->getData();
          
           $manager->persist($category);
       
           $manager->flush();

           $this->addFlash('success','Catégorie ajoutée avec succès');
           return $this->redirectToRoute('category.add');

        }

        return $this->render('category/index.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
