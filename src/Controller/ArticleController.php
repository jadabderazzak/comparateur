<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[IsGranted('ROLE_USER')]
class ArticleController extends AbstractController
{
    #[Route('/article', name: 'article.index')]
    public function index(): Response
    {
        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
        ]);
    }

    #[Route('/article/add', name: 'article.add')]
    public function add(Request $request, EntityManagerInterface $manager): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class,$article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
           $article = $form->getData();
        
           $article->setDescription("description");
           
          
           $manager->persist($article);
       
           $manager->flush();

           $this->addFlash('success','Article ajouté avec succès');
           return $this->redirectToRoute('article.add');

        }

        return $this->render('article/index.html.twig',[
            'form' => $form->createView()
        ]);
    }

    
}
