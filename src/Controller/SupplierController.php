<?php

namespace App\Controller;

use App\Entity\Supplierarticle;
use App\Form\ArticlesupplierFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[IsGranted('ROLE_USER')]
class SupplierController extends AbstractController
{
    #[Route('/supplier', name: 'app_supplier')]
    public function index(): Response
    {
        return $this->render('supplier/index.html.twig', [
            'controller_name' => 'SupplierController',
        ]);
    }

    #[Route('/supplier/article/add', name: 'supplier.article.add')]
    public function add(Request $request, EntityManagerInterface $manager): Response
    {
        $article = new Supplierarticle();
        $form = $this->createForm(ArticlesupplierFormType::class,$article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
           $article = $form->getData();
        
           $article->setUser($this->getUser());
           
         

           $manager->persist($article);
       
           $manager->flush();

           $this->addFlash('success','Article ajouté avec succès');
           return $this->redirectToRoute('supplier.article.add');

        }

        return $this->render('supplier/index.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
