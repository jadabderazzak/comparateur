<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class,[
                'required' => false,
                'attr' => [
                    'placeholder' => 'Veuillez saisir le nom de l\'article'
                ]
            ])
           
            ->add('details',TextareaType::class,[
                'required' => false,
                'attr' => ['class' => 'tinymce', 'placeholder' => 'Veuillez donner plus de détails sur cet article'],
            ])
           
            ->add('imageFile',VichImageType::class,[
                'required' => false,
            ])
            ->add('category', EntityType::class, [
                'required' => true,
                'class' => Category::class,
                'placeholder' => 'Veuillez choisir une catégorie',
                'choice_label' => 'nom', 
          
            
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
