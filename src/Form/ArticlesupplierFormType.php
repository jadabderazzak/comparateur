<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Supplierarticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ArticlesupplierFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('article',EntityType::class, [
            'required' => true,
            'class' => Article::class,
            'placeholder' => 'Veuillez choisir un article',
            'choice_label' => 'nom', 
      
        
        ])
        ->add('prix',NumberType::class,[
            'required' => false,
            'attr' => [
                'placeholder' => 'Veuillez saisir un prix '
        ]])
      
        ->add('lien',TextType::class,[
            'required' => false,
            'attr' => [
                'placeholder' => 'Veuillez saisir le lien vers cet article'
            ]
        ])
       
            ->add('livraison',CheckboxType::class,[
                'required' => false,
                'data' => true,
                'label' => 'La livraison est elle gratuite ?'
                  
            ])
            ->add('prixLivraison',NumberType::class,[
                'required' => false,
                'attr' => [
                    'placeholder' => 'Entrer le prix de la livraison si elle n\'est pas gratuite.'
            ]])
            
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Supplierarticle::class,
        ]);
    }
}
